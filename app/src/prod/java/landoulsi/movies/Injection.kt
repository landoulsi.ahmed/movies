package landoulsi.movies

import landoulsi.movies.rest.MoviesDataSource
import landoulsi.movies.rest.NetworkHandler
import landoulsi.movies.util.schedulers.BaseSchedulerProvider
import landoulsi.movies.util.schedulers.SchedulerProvider


object Injection {

    fun provideMoviesRepository(): MoviesDataSource {
        return NetworkHandler.dataSource
    }

    fun provideSchedulerProvider(): BaseSchedulerProvider {
        return SchedulerProvider
    }
}