package landoulsi.movies.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    val DEFAULT_DATE_FORMAT = "yyyy-MM-dd"

    fun formatDate(date: Date?, dateFormat: String = DEFAULT_DATE_FORMAT): String {
        if (date == null) return ""
        val format = SimpleDateFormat(dateFormat, Locale.ENGLISH)
        return format.format(date)
    }

    val lastYearDate: Date
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.YEAR, -1)
            return calendar.time
        }

    val lastMonthDate: Date
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.MONTH, -1)
            return calendar.time
        }

    val lastWeekDate: Date
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.WEEK_OF_MONTH, -1)
            return calendar.time
        }
}