package landoulsi.movies.util

import android.util.Log

object Logger {

    var DEBUG = true

    fun d(tag: String, msg: Any?) {
        if (DEBUG)
            Log.d(tag, "" + msg)
    }

    fun i(tag: String, msg: Any?) {
        if (DEBUG)
            Log.i(tag, "" + msg)
    }

    fun w(tag: String, msg: Any?) {
        if (DEBUG)
            Log.w(tag, "" + msg)
    }

    fun i(tag: String, msg: Any?, tr: Throwable) {
        if (DEBUG)
            Log.i(tag, "" + msg, tr)
    }

    fun w(tag: String, tr: Throwable) {
        if (DEBUG)
            Log.w(tag, tr)
    }

    fun e(tag: String, e: Exception) {
        if (DEBUG) {
            // TODO upload crash to crashlytics
            Log.e(tag, e.toString())
        }
    }

    fun e(tag: String, throwable: Throwable) {
        if (DEBUG) {
            // TODO upload crash to crashlytics
            Log.e(tag, "", throwable)
        }
    }
}
