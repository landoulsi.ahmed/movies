package landoulsi.movies.view

import landoulsi.movies.model.MovieModel

interface MoviesView : DataView<List<MovieModel>> {

    fun showMovieDetails(movieModel: MovieModel)

    fun showNoDataAvailable()

    fun hasNoData(): Boolean
}