package landoulsi.movies.view

interface BaseView {
    val isActive: Boolean
}