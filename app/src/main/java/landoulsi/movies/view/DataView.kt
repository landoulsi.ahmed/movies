package landoulsi.movies.view

interface DataView<in DataType> : BaseView {

    fun onDataSuccess(data: DataType)

    fun onDataError(throwable: Throwable)

    fun showProgress(showProgress: Boolean)
}