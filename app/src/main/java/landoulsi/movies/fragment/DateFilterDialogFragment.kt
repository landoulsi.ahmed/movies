package landoulsi.movies.fragment

import android.view.LayoutInflater
import android.view.View
import landoulsi.movies.databinding.FragmentDialogDateFilterBinding
import landoulsi.movies.model.DateFilterType

open class DateFilterDialogFragment : BaseBottomSheetDialogFragment() {

    private var bindingView: FragmentDialogDateFilterBinding? = null
    var callback: Callback? = null
    var dateFilter: DateFilterType? = null

    override fun getBindingView(inflater: LayoutInflater): View {
        bindingView = FragmentDialogDateFilterBinding.inflate(inflater)
        return bindingView!!.root
    }

    override fun initViews() {
        bindingView!!.all.setOnClickListener {
            callback?.onFilterSelected(DateFilterType.ALL)
        }
        bindingView!!.lastYear.setOnClickListener({ v ->
            callback?.onFilterSelected(DateFilterType.LAST_YEAR)
        })
        bindingView!!.lastMonth.setOnClickListener({ v ->
            callback?.onFilterSelected(DateFilterType.LAST_MONTH)
        })
        bindingView!!.lastWeek.setOnClickListener({ v ->
            callback?.onFilterSelected(DateFilterType.LAST_WEEK)
        })

        if (dateFilter == null || dateFilter === DateFilterType.ALL) {
            bindingView!!.all.isChecked = true
        } else {
            when (dateFilter) {
                DateFilterType.LAST_YEAR -> bindingView!!.lastYear.isChecked = true
                DateFilterType.LAST_MONTH -> bindingView!!.lastMonth.isChecked = true
                DateFilterType.LAST_WEEK -> bindingView!!.lastWeek.isChecked = true
            }
        }
    }

    interface Callback {
        fun onFilterSelected(dateFilterType: DateFilterType)
    }
}