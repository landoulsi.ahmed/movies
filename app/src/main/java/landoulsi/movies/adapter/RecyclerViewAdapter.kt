package landoulsi.movies.adapter

import android.content.ContentValues.TAG
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import landoulsi.movies.widget.BaseWidget


open class RecyclerViewAdapter<T>(private val context: Context,
                                  private val vClass: Class<out BaseWidget<T>>,
                                  var listItems: List<T>) :
        RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder<T, BaseWidget<T>>>() {

    var onItemClickListener: OnItemClickListener<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder<T, *>? {
        val convertView: BaseWidget<T>
        try {
            val constructor = vClass.getConstructor(Context::class.java)
            convertView = constructor.newInstance(context)
            convertView.initLayout(viewType)
            return RecyclerViewHolder(convertView)
        } catch (e: Exception) {
            Log.e(TAG, "", e)
        }
        return null
    }

    private val clickListener = View.OnClickListener { view ->
        if (onItemClickListener != null) {
            val position = (view.tag as RecyclerViewHolder<*, *>).getAdapterPosition()
            onItemClickListener?.onItemClick(this, view, getItem(position), position)
        }
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder<T, BaseWidget<T>>?, position: Int) {
        holder?.view?.setInitData(listItems[position])
        holder?.view?.tag = holder
        holder?.view?.setOnClickListener(clickListener)
    }

    private fun getItem(position: Int): T {
        return listItems[position]
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    class RecyclerViewHolder<T, out V : BaseWidget<T>>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: V
            get() = itemView as V
    }

    interface OnItemClickListener<T> {
        fun onItemClick(adapter: RecyclerViewAdapter<T>, view: View, item: T, position: Int)
    }
}
