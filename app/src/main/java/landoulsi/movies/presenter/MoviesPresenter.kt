package landoulsi.movies.presenter

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.functions.ObjectHelper.requireNonNull
import landoulsi.movies.model.DateFilterType
import landoulsi.movies.model.MovieModel
import landoulsi.movies.rest.MoviesDataSource
import landoulsi.movies.util.DateUtil
import landoulsi.movies.util.schedulers.BaseSchedulerProvider
import landoulsi.movies.view.MoviesView
import java.util.*

open class MoviesPresenter(dataSource: MoviesDataSource, schedulerProvider: BaseSchedulerProvider, view: MoviesView) :
        BasePresenter<MoviesView>(CompositeDisposable(), view) {

    private val mMoviesDataSource: MoviesDataSource = requireNonNull<MoviesDataSource>(dataSource, null)
    private val mSchedulerProvider: BaseSchedulerProvider = requireNonNull(schedulerProvider, null)
    var mMovieList: MutableList<MovieModel> = ArrayList()
    var dateFilter: DateFilterType? = null

    fun loadMovies(page: Int) {
        view?.showProgress(true)

        val subscription = mMoviesDataSource.getMovies(page, minReleaseDate)
                .map { movieListResponse -> movieListResponse.movieModels }
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe({ this.processMovies(it) }, { this.processError(it) })
        mSubscriptions.add(subscription)
    }

    val minReleaseDate: String?
        get() {
            if (dateFilter != null && dateFilter !== DateFilterType.ALL) {
                var minDate: Date? = null

                when (dateFilter) {
                    DateFilterType.LAST_YEAR -> minDate = DateUtil.lastYearDate
                    DateFilterType.LAST_MONTH -> minDate = DateUtil.lastMonthDate
                    DateFilterType.LAST_WEEK -> minDate = DateUtil.lastWeekDate
                }
                if (minDate != null) {
                    return DateUtil.formatDate(minDate)
                }
            }
            return null
        }

    private fun processError(throwable: Throwable) {
        view?.showProgress(false)
        view?.onDataError(throwable)
    }

    private fun processMovies(movieModels: List<MovieModel>?) {
        view?.showProgress(false)
        if (movieModels == null || movieModels.isEmpty()) {
            view?.showNoDataAvailable()
        } else {
            mMovieList.addAll(movieModels)
            view?.onDataSuccess(mMovieList)
        }
    }

    fun openMovieDetails(movieModel: MovieModel) {
        requireNonNull(movieModel, "MovieModel cannot be null!")
        view?.showMovieDetails(movieModel)
    }

    fun clear() {
        mMovieList.clear()
    }

    fun hasNoData(): Boolean {
        return mMovieList.isEmpty()
    }
}