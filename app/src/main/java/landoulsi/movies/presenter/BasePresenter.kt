package landoulsi.movies.presenter

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.functions.ObjectHelper.requireNonNull
import landoulsi.movies.view.DataView

open class BasePresenter<T : DataView<*>>(subscriptions: CompositeDisposable, view: T) {

    protected var mSubscriptions: CompositeDisposable = requireNonNull(subscriptions, null)
    protected var view: T? = requireNonNull(view, null)

    fun subscribe() {}

    fun unsubscribe() {
        mSubscriptions.clear()
    }

    val isViewAvailable: Boolean
        get() = view != null

    fun onDestroy() {
        this.view = null
    }
}