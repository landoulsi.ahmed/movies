package landoulsi.movies

import android.app.Application

import landoulsi.movies.util.Logger

class MovieApp : Application() {

    private val APP_EXIT_CODE_FOR_CRASH = 2

    private val exceptionHandler = { _: Thread, e: Throwable ->
        Logger.e(MovieApp::class.java.name, e)
        System.exit(APP_EXIT_CODE_FOR_CRASH)
    }

    override fun onCreate() {
        super.onCreate()
        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler)
    }
}