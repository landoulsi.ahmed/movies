package landoulsi.movies.model

import android.annotation.SuppressLint
import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import io.mironov.smuggler.AutoParcelable
import landoulsi.movies.util.AppData
import java.util.*

@SuppressLint("ParcelCreator")
class MovieModel(@SerializedName("overview") var overview: String?,
                 @SerializedName("release_date") var releaseDate: Date?,
                 @SerializedName("poster_path") private var posterPath: String? = null)
    : AutoParcelable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("original_title")
    private var originalTitle: String? = null

    @SerializedName("original_language")
    private var originalLanguage: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("backdrop_path")
    private var backdropPath: String? = null

    @SerializedName("popularity")
    private var popularity: String? = null

    @SerializedName("vote_count")
    private var voteCount: Int = 0

    @SerializedName("video")
    private var video: Boolean = false

    @SerializedName("vote_average")
    private var voteAverage: Float = 0.toFloat()

    fun getPosterPath(): String? {
        if (!TextUtils.isEmpty(posterPath)) {
            return AppData.baseThumbnailUrl + posterPath!!
        }
        return null
    }
}