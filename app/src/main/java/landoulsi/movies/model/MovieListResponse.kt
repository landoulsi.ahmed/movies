package landoulsi.movies.model

import com.google.gson.annotations.SerializedName

class MovieListResponse(@SerializedName("page")
                        private val page: Int, @SerializedName("results")
                        val movieModels: List<MovieModel>)