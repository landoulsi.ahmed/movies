package landoulsi.movies.model

enum class DateFilterType {
    ALL, LAST_YEAR, LAST_MONTH, LAST_WEEK
}