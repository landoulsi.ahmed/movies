package landoulsi.movies.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout

abstract class BaseWidget<T> : FrameLayout {

    protected val TAG: String = javaClass.simpleName
    var data: T? = null
    private val NO_TYPE = 0
    private val position: Int = 0

    constructor(context: Context) : super(context) {
        if (!isListRow) {
            initLayout(NO_TYPE)
            setInitData(null)
        }
    }

    @JvmOverloads constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int = 0)
            : super(context, attrs, defStyleAttr) {
        initLayout(NO_TYPE)
        setInitData(null)
        processAttributes(attrs, defStyleAttr)
    }

    protected fun processAttributes(attrs: AttributeSet?, defStyleAttr: Int) {}

    fun setInitData(data_: T?) {
        data = data_
        initViews()
    }

    fun initConfig() {}

    fun initData() {}

    protected val isListRow: Boolean
        get() = false

    fun initLayout(type: Int) {
        initConfig()
        getBindingView(type)
        initData()
    }

    val layoutInflater: LayoutInflater
        get() = LayoutInflater.from(context)

    protected abstract fun getBindingView(type: Int)

    protected abstract fun initViews()
}
