package landoulsi.movies.widget

import android.content.Context
import android.util.AttributeSet
import com.squareup.picasso.Picasso
import landoulsi.movies.databinding.ViewMovieItemBinding
import landoulsi.movies.model.MovieModel
import landoulsi.movies.util.DateUtil

class MovieItemView : BaseWidget<MovieModel> {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var viewBinder: ViewMovieItemBinding? = null

    override fun getBindingView(type: Int) {
        viewBinder = ViewMovieItemBinding.inflate(layoutInflater, this, true)
    }

    override fun initViews() {
        if (data != null) {
            viewBinder!!.title.text = data!!.title
            viewBinder!!.description.text = data!!.overview
            viewBinder!!.date.text = DateUtil.formatDate(data!!.releaseDate)
            Picasso.with(context).load(data!!.getPosterPath()).into(viewBinder!!.image)
        }
    }
}