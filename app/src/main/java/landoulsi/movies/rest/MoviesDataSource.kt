package landoulsi.movies.rest

import io.reactivex.Observable
import landoulsi.movies.model.MovieListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesDataSource {

    @GET("movie")
    fun getMovies(@Query("page") page: Int,
                  @Query("release_date.gte") minReleaseDate: String?):
            Observable<MovieListResponse>
}