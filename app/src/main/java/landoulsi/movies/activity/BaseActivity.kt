package landoulsi.movies.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bindingView)
        initViews()
    }

    protected abstract val bindingView: View

    protected abstract fun initViews()
}