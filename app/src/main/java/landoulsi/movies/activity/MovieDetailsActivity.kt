package landoulsi.movies.activity

import android.view.View
import android.widget.Toast

import com.squareup.picasso.Picasso
import landoulsi.movies.databinding.ActivityMovieDetailsBinding
import landoulsi.movies.model.MovieModel
import landoulsi.movies.util.DateUtil

class MovieDetailsActivity : BaseActivity() {
    private var viewBinder: ActivityMovieDetailsBinding? = null

    companion object {
        val EXTRA_MOVIE = "extra_movie"
    }

    override val bindingView: View
        get() {
            viewBinder = ActivityMovieDetailsBinding.inflate(layoutInflater)
            return viewBinder!!.root
        }

    override fun initViews() {
        val movieModel = intent.getParcelableExtra<MovieModel>(EXTRA_MOVIE)

        if (movieModel == null) {
            finish()
            Toast.makeText(this, "Error during loading the details screen", Toast.LENGTH_SHORT).show()
            return
        }

        viewBinder!!.title.text = movieModel.title
        viewBinder!!.description.text = movieModel.overview
        viewBinder!!.date.text = DateUtil.formatDate(movieModel.releaseDate)
        Picasso.with(this).load(movieModel.getPosterPath()).into(viewBinder!!.image)
    }
}