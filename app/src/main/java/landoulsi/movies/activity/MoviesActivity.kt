package landoulsi.movies.activity

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import landoulsi.movies.Injection
import landoulsi.movies.R
import landoulsi.movies.adapter.RecyclerViewAdapter
import landoulsi.movies.callback.EndlessScrollListener
import landoulsi.movies.databinding.ActivityMoviesBinding
import landoulsi.movies.fragment.DateFilterDialogFragment
import landoulsi.movies.model.DateFilterType
import landoulsi.movies.model.MovieModel
import landoulsi.movies.presenter.MoviesPresenter
import landoulsi.movies.view.MoviesView
import landoulsi.movies.widget.MovieItemView

class MoviesActivity : BaseActivity(), MoviesView, DateFilterDialogFragment.Callback {

    private val FIRST_PAGE = 1

    private var mBinder: ActivityMoviesBinding? = null
    private var mPresenter: MoviesPresenter? = null
    private var fragment: DateFilterDialogFragment? = null
    private var adapter: RecyclerViewAdapter<MovieModel>? = null

    override val bindingView: View
        get() {
            mBinder = ActivityMoviesBinding.inflate(layoutInflater)
            return mBinder!!.root
        }

    override fun initViews() {
        fragment = DateFilterDialogFragment()
        fragment!!.callback = this

        mPresenter = MoviesPresenter(Injection.provideMoviesRepository(),
                Injection.provideSchedulerProvider(), this)

        val layoutManager = LinearLayoutManager(this)
        mBinder!!.recyclerView.layoutManager = layoutManager
        val listener = object : EndlessScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                loadData(page + 1)
            }
        }

        mBinder!!.recyclerView.addOnScrollListener(listener)
        adapter = RecyclerViewAdapter(this, MovieItemView::class.java, mPresenter!!.mMovieList)
        mBinder!!.recyclerView.adapter = adapter

        adapter?.onItemClickListener = object : RecyclerViewAdapter.OnItemClickListener<MovieModel> {
            override fun onItemClick(adapter: RecyclerViewAdapter<MovieModel>, view: View, item: MovieModel, position: Int) {
                mPresenter!!.openMovieDetails(item)
            }
        }

        mBinder!!.swipeRefresh.setOnRefreshListener {
            clearData()
            loadData(FIRST_PAGE)
        }
        loadData(FIRST_PAGE)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_movies, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.date_filter -> {
                showDateFilterDialog()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showDateFilterDialog() {
        fragment!!.dateFilter = mPresenter?.dateFilter
        fragment!!.show(supportFragmentManager, DateFilterDialogFragment::class.java.name)
    }

    private fun loadData(page: Int) {
        mBinder!!.noDataAvailable.visibility = View.GONE
        mPresenter!!.loadMovies(page)
    }

    public override fun onResume() {
        super.onResume()
        mPresenter!!.subscribe()
    }

    public override fun onPause() {
        super.onPause()
        mPresenter!!.unsubscribe()
    }

    override fun onDestroy() {
        mPresenter!!.onDestroy()
        super.onDestroy()
    }

    override fun onDataSuccess(data: List<MovieModel>) {
        adapter?.listItems = data
        adapter?.notifyDataSetChanged()
    }

    override fun onDataError(throwable: Throwable) {
        Toast.makeText(this@MoviesActivity,
                getString(R.string.server_error), Toast.LENGTH_SHORT).show()
    }

    override fun showProgress(showProgress: Boolean) {
        mBinder!!.swipeRefresh.isRefreshing = showProgress
    }

    override val isActive: Boolean
        get() = !isFinishing

    override fun showMovieDetails(movieModel: MovieModel) {
        val intent = Intent(this@MoviesActivity, MovieDetailsActivity::class.java)
        intent.putExtra(MovieDetailsActivity.EXTRA_MOVIE, movieModel)
        startActivity(intent)
    }

    override fun showNoDataAvailable() {
        if (hasNoData()) {
            mBinder!!.noDataAvailable.visibility = View.VISIBLE
        }
    }

    override fun hasNoData(): Boolean {
        return mPresenter!!.hasNoData()
    }

    override fun onFilterSelected(dateFilterType: DateFilterType) {
        if (fragment != null) {
            fragment!!.dismiss()
        }
        mPresenter!!.dateFilter = dateFilterType
        clearData()
        loadData(FIRST_PAGE)
    }

    private fun clearData() {
        mPresenter!!.clear()
        adapter?.notifyDataSetChanged()
    }
}