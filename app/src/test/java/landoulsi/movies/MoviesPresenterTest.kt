package landoulsi.movies

import io.reactivex.Observable
import landoulsi.movies.model.MovieListResponse
import landoulsi.movies.model.MovieModel
import landoulsi.movies.presenter.MoviesPresenter
import landoulsi.movies.rest.MoviesDataSource
import landoulsi.movies.util.schedulers.ImmediateSchedulerProvider
import landoulsi.movies.view.MoviesView
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.*

class MoviesPresenterTest {

    @Mock
    private val moviesDataManager: MoviesDataSource? = null

    @Mock
    private val mMoviesView: MoviesView? = null

    @Mock
    private var mMoviesPresenter: MoviesPresenter? = null
    private var MOVIES: List<MovieModel>? = null

    @Before
    fun setupMoviesPresenter() {
        MockitoAnnotations.initMocks(this)
        val mSchedulerProvider = ImmediateSchedulerProvider()
        mMoviesPresenter = MoviesPresenter(moviesDataManager!!, mSchedulerProvider, mMoviesView!!)
        `when`(mMoviesView.isActive).thenReturn(true)
        MOVIES = Arrays.asList(MovieModel("Title1", Date()),
                MovieModel("Title2", Date()), MovieModel("Title3", Date()))
    }

    @Test
    fun loadCompletedMovie_LoadIntoView() {
        val movieListResponse = MovieListResponse(0, MOVIES!!)
        `when`(moviesDataManager!!.getMovies(1, null))
                .thenReturn(Observable.just(movieListResponse))

        mMoviesPresenter!!.loadMovies(1)

        verify<MoviesView>(mMoviesView).showProgress(true)
        verify<MoviesView>(mMoviesView).showProgress(false)
        val inOrder = Mockito.inOrder(mMoviesView)
        inOrder.verify<MoviesView>(mMoviesView, times(1)).onDataSuccess(movieListResponse.movieModels)
        verify<MoviesView>(mMoviesView, never()).showNoDataAvailable()
    }

    @Test
    fun emptyMovies_showNoData() {
        val movieListResponse = MovieListResponse(0, ArrayList<MovieModel>())
        `when`(moviesDataManager!!.getMovies(1, null))
                .thenReturn(Observable.just(movieListResponse))

        mMoviesPresenter!!.loadMovies(1)

        verify<MoviesView>(mMoviesView).showProgress(true)
        verify<MoviesView>(mMoviesView).showProgress(false)

        val inOrder = Mockito.inOrder(mMoviesView)
        inOrder.verify<MoviesView>(mMoviesView, times(1)).showNoDataAvailable()
        verify<MoviesView>(mMoviesView, never()).onDataSuccess(MOVIES!!)
    }

    @Test
    fun errorLoadingMovies_ShowsError() {
        val exception = Exception()

        `when`(moviesDataManager!!.getMovies(1, null))
                .thenReturn(Observable.error<MovieListResponse>(exception))

        mMoviesPresenter!!.loadMovies(1)

        val inOrder = Mockito.inOrder(mMoviesView)
        inOrder.verify<MoviesView>(mMoviesView, times(1)).onDataError(exception)
        verify<MoviesView>(mMoviesView, never()).onDataSuccess(MOVIES!!)
    }

    @Test
    fun clickOnMovie_ShowsDetail() {
        val movieModel = MovieModel("Title1", Date())

        mMoviesPresenter!!.openMovieDetails(movieModel)
        verify<MoviesView>(mMoviesView).showMovieDetails(movieModel)
    }
}